package section3;

import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		String artikel;
		int anzahl;
		double preis, mwst;
		
		// Eingabe:
		// =======
		System.out.println("Was möchten Sie bestellen?");
		artikel = liesString(myScanner.next());

		System.out.println("Geben Sie die Anzahl ein:");
		anzahl = liesInt(myScanner.next());

		System.out.println("Geben Sie den Nettopreis ein:");
		preis = liesDouble(myScanner.next());

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		mwst = liesDouble(myScanner.next());

		// Verarbeiten:
		// ===========
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgabe:
		// =======
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}

	public static String liesString(String text) {
		return text;
	}
	
	public static int liesInt(String text) {
		return Integer.parseInt(text);
	}
	
	public static double liesDouble(String text) {
		return Double.parseDouble(text);
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.printf("\tRechnung\t %-20s %6s %10s\n", "Artikel", "Anzahl", "Preis");
		System.out.printf("\t\t\t %-20s %6s %10s\n", "-------", "------", "-----");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	
}