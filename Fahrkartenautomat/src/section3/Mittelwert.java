package section3;

import utils.Tastatur;

public class Mittelwert {

    public static void main(String[] args) {
        // (E) "Eingabe"
        // Werte f�r x und y festlegen:
        // ===========================
    	System.out.print("Wie viele Zahlen sollen berechnet werden? ");
    	int anzahl = Tastatur.liesInt();
        double m, summe = 0;
        int zaehlschleife = 0;

        // (V) Verarbeitung
        // Mittelwert von x und y berechnen:
        // ================================
        while (zaehlschleife++ < anzahl) {
        	System.out.print("Bitte gebe eine Zahl an: ");
        	summe += Tastatur.liesDouble();
        }
        
        m = summe / anzahl;

        // (A) Ausgabe
        // Ergebnis auf der Konsole ausgeben:
        // =================================
        System.out.printf("Der Mittelwert ist %.2f\n", m);
    }
}
