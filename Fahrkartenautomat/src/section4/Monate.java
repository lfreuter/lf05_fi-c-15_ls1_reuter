package section4;

import java.util.Scanner;

public class Monate {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Gebe eine Monatszahl an: ");
		int monat = scanner.nextInt();
		
		switch (monat) {
			case 1:
				System.out.println("Das ist der " + monat + ". Monat: Januar");
				break;
			case 2:
				System.out.println("Das ist der " + monat + ". Monat: Februar");
				break;
			case 3:
				System.out.println("Das ist der " + monat + ". Monat: M�rz");
				break;
			case 4:
				System.out.println("Das ist der " + monat + ". Monat: April");
				break;
			case 5:
				System.out.println("Das ist der " + monat + ". Monat: Mai");
				break;
			case 6:
				System.out.println("Das ist der " + monat + ". Monat: Juni");
				break;
			case 7:
				System.out.println("Das ist der " + monat + ". Monat: Juli");
				break;
			case 8:
				System.out.println("Das ist der " + monat + ". Monat: August");
				break;
			case 9:
				System.out.println("Das ist der " + monat + ". Monat: September");
				break;
			case 10:
				System.out.println("Das ist der " + monat + ". Monat: Oktober");
				break;
			case 11:
				System.out.println("Das ist der " + monat + ". Monat: November");
				break;
			case 12:
				System.out.println("Das ist der " + monat + ". Monat: Dezember");
				break;
			default:
				System.err.println("Die Monatszahl ist nicht m�glich");
				break;
		}
	}

}
