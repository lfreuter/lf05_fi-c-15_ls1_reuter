package section4;

import java.util.Scanner;

public class Noten {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Gebe eine Note an: ");
		int note = scanner.nextInt();
		
		switch (note) {
			case 1:
				System.out.println("Die Note " + note + " ist Sehr gut");
				break;
			case 2:
				System.out.println("Die Note " + note + " ist Gut");
				break;
			case 3:
				System.out.println("Die Note " + note + " ist Befriedigend");
				break;
			case 4:
				System.out.println("Die Note " + note + " ist Ausreichend");
				break;
			case 5:
				System.out.println("Die Note " + note + " ist Mangelhaft");
				break;
			case 6:
				System.out.println("Die Note " + note + " ist Ungenügend");
				break;
			default:
				System.err.println("Die Note existiert nicht");
				break;
		}
	}
	
}
