package section4;

import java.util.Scanner;

public class Steuersatz {
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Gebe den Nettowert an: ");
		int nettoWert = myScanner.nextInt();
		
		System.out.print("Ermäßigter(j) oder voller(n) Steuersatz: ");
		char steuersatz = myScanner.next().charAt(0);
		
		switch (steuersatz) {
			case 'j': //Ermäßigter
				System.out.println("Beim ermäßigten Steuersatz liegt der Bruttobetrag bei: " + (nettoWert + (nettoWert * 0.07F)));
				break;
			case 'n': //Voller
				System.out.println("Beim vollen Steuersatz liegt der Bruttobetrag bei: " + (nettoWert + (nettoWert * 0.19F)));
				break;
		}
	}
}