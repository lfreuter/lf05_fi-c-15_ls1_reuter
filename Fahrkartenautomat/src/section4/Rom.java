package section4;

import java.util.Arrays;
import java.util.Scanner;

public class Rom {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Geben Sie R�mische Zahlen an: ");
		String zahlen = scanner.next();
		int result = 0;
		for (char zahl : zahlen.toCharArray()) {
			switch (zahl) {
				case 'I':
					result+=1;
					break;
				case 'V':
					result+=5;
					break;
				case 'X':
					result+=10;
					break;
				case 'L':
					result+=50;
					break;
				case 'C':
					result+=100;
					break;
				case 'D':
					result+=500;
					break;
				case 'M':
					result+=1000;
					break;
				default:
					System.err.println("Es wurde eine ung�ltige Zahl angegeben.");
					break;
			}
		}
		System.out.println(zahlen + " ergibt " + result);
	}
	
}
