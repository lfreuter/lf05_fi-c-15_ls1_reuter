package section4;

import utils.Tastatur;

public class Zaehlen {

	public static void main(String[] args) {
		
		int a = Tastatur.liesInt(), counta = 0;
		while (counta++ < a) {
			System.out.print(counta + ", ");
		}
		System.out.println();
		while (counta-- > 1) {
			System.out.print(counta + ", ");
		}
	}
	
}
