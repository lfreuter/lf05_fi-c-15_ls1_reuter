package section4;

import java.util.Scanner;

public class Verzweigung2 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		double zahl1, zahl2, zahl3, largest;
		boolean greaterThanOthers, greaterThenOne;
		
		zahl1 = eingabe(scanner);
		zahl2 = eingabe(scanner);
		zahl3 = eingabe(scanner);
		
		System.out.print("Welche Aufgabe soll genutzt werden (1, 2, 3): ");
		
		switch (scanner.next()) {
			case "1":
				greaterThanOthers = isGreaterThenTheOthers(zahl1, zahl2, zahl3);
				if (greaterThanOthers) 
					System.out.println("👍");
				break;
			case "2":
				greaterThenOne = isGreaterThenOne(zahl1, zahl2, zahl3);
				if (greaterThenOne) 
					System.out.println("👍");
				break;
			case "3":
				largest = getLargest(zahl1, zahl2, zahl3);
				System.out.println(largest);
				break;
		}
	}
	
	private static double eingabe(Scanner scanner) {
		double zahl;
		System.out.println("Gebe eine Zahl an: ");
		zahl = scanner.nextDouble();
		
		return zahl;
	}

	private static boolean isGreaterThenTheOthers(double zahl1, double zahl2, double zahl3) {
		return (zahl1 > zahl2 && zahl1 > zahl3);
	}
	
	private static boolean isGreaterThenOne(double zahl1, double zahl2, double zahl3) {
		return (zahl3 > zahl2 || zahl1 < zahl3);
	}
	
	private static double getLargest(double zahl1, double zahl2, double zahl3) {
		double temp = zahl1 > zahl2 ? zahl1 : zahl2;
		return zahl3 > temp ? zahl3 : temp;
	}
	
}
