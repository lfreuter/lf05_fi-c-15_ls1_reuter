package section4;

import java.util.Scanner;

public class Rechner {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int zahl1, zahl2;
		char operation;
		
		System.out.print("Gebe eine Zahl an: ");
		zahl1 = scanner.nextInt();
		
		System.out.print("Gebe eine 2. Zahl an: ");
		zahl2 = scanner.nextInt();
		
		System.out.print("Gebe eine Operation an: ");
		operation = scanner.next().charAt(0);
		
		switch (operation) {
			case '/':
				System.out.println(zahl1 + " " + operation + " " + zahl2 + " = " + (zahl1/zahl2));
				break;
			case '*':
				System.out.println(zahl1 + " " + operation + " " + zahl2 + " = " + (zahl1*zahl2));
				break;
			case '+':
				System.out.println(zahl1 + " " + operation + " " + zahl2 + " = " + (zahl1+zahl2));
				break;
			case '-':
				System.out.println(zahl1 + " " + operation + " " + zahl2 + " = " + (zahl1-zahl2));
				break;
			default:
				System.err.println("Die Operation ist nicht m�glich");
				break;
		}
	}
}
