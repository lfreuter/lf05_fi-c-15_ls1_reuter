package section4;

import java.util.Arrays;
import java.util.Scanner;

public class Sortiere_abc {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char input1, input2, input3;
        char[] sort;
        
        input1 = getInput(scanner);
        input2 = getInput(scanner);
        input3 = getInput(scanner);
        sort = sort(input1, input2, input3);

        System.out.println("Die Sortierung der Zeichen ist: " + sort[0] + ", " + sort[1] + ", " + sort[2]);
        //Arrays.sort(input);
        //System.out.println(input);
    }

    public static char getInput(Scanner scanner) {
        System.out.print("Gebe drei Zeichen an (ohne trennung): ");
        return scanner.next().charAt(0);
    }

    public static char[] sort(char first, char second, char third) {
        char a, b, c;
        a = (first < second ? first < third ? first : third : (second < third ? second : third));
        b = (second < first ? second > third ? second : (third < first ? third : first) : (second < third ? second : first > third ? first : third));
        c = (third > first ? third > second ? third : second : (first > second ? first : second));
        return new char[] { a, b, c };
    }
}
