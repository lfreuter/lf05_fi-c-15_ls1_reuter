package section4;

import utils.Tastatur;

public class Treppe {

	public static void main(String[] args) {
		byte h, b;
		String out = "";
		
		System.out.print("H�he (h): ");
		h = Tastatur.liesByte();
		
		System.out.print("Breite (b): ");
		b = Tastatur.liesByte();
		
		for (byte breite = 0; breite < b; breite++)
			out += "*";
		
		for (byte count = h; count-- > 0;) {
			System.out.printf("%" + (b * count + out.length()) + "s%n", out);
			for (byte breite = 0; breite < b; breite++)
				out += "*";
		}
	}
}