package section4;

import java.util.Scanner;

public class Verzweigung {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		double zahl1, zahl2;
		boolean equal, greater, equalOrGreater;
		
		zahl1 = eingabe(scanner);
		zahl2 = eingabe(scanner);
		
		System.out.print("Welche Aufgabe soll genutzt werden (2, 3, 4): ");
		
		switch (scanner.next()) {
			case "2":
				equal = isEqual(zahl1, zahl2);
				
				if (equal)
					System.out.print("Aufgabe 1.2: " + zahl1 + " == " + zahl2);
				break;
			case "3":
				greater = isGreater(zahl1, zahl2);
				
				if (greater) 
					System.out.print("Aufgabe 1.3: " + zahl1 + " < " + zahl2);
				break;
			case "4":
				equalOrGreater = isEqualOrGreater(zahl1, zahl2);
				
				if (equalOrGreater)
					System.out.print("Aufgabe 1.4: " + zahl1 + " >= " + zahl2);
				else 
					System.out.print("Aufgabe 1.4: " + zahl1 + " < " + zahl2);
				break;
		}
	}
	
	private static double eingabe(Scanner scanner) {
		double zahl;
		System.out.println("Gebe eine Zahl an: ");
		zahl = scanner.nextInt();
		
		return zahl;
	}
	
	private static boolean isEqual(double zahl1, double zahl2) {
		return (zahl1 == zahl2);
	}
	
	private static boolean isGreater(double zahl1, double zahl2) {
		return zahl1 < zahl2;
	}

	private static boolean isEqualOrGreater(double zahl1, double zahl2) {
		return zahl1 >= zahl2;
	}

}
