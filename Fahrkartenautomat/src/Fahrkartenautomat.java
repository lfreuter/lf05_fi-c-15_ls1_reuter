import java.util.Scanner;

class Fahrkartenautomat {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag, eingezahlterGesamtbetrag;
		byte tickets;
		
		zuZahlenderBetrag = fahrkartenPreis(tastatur);
		tickets = fahrkartenbestellungErfassen(tastatur);
		zuZahlenderBetrag *= tickets;
		eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
		
		fahrkartenAusgeben(tickets);
		rueckgeldAusgeben(eingezahlterGesamtbetrag - zuZahlenderBetrag, tickets);
	}
	
	public static byte fahrkartenbestellungErfassen(Scanner tastatur) {
		System.out.print("Anzahl der Tickets: ");
		// Anzahl an Tickets kann keine Komma Zahl sein und h�chstwahrscheinlich klein ist, deswegen byte
		byte tickets = tastatur.nextByte();
		if (tickets > 10 || tickets < 1) { 
			tickets = 1;
			System.out.println("Die Anzahl der Tickets kann nur zwischen 1 und 10 liegen.\n"
					+ "Die Tickets wurden auf 1 gesetzt.");
		}
		return tickets;
	}
	
	public static double fahrkartenPreis(Scanner tastatur) {
		System.out.print("Ticketpreis (EURO-Cent): ");
		double preis = tastatur.nextDouble();
		if (preis < 0) {
			preis = 1.5;
			System.out.println("Fehler: Preis unzul�ssig.\n"
					+ "Der Preis wurde auf 1,50� gesetzt.");
		}
		return preis;
	}
	
	public static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %4.2f � %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben(byte tickets) {
		System.out.println("\n" + (tickets > 1 ? "Fahrscheine werden" : "Fahrschein wird") + " ausgegeben");
		System.out.print("+");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.print("+");
		System.out.println("\n");
	}
	
	public static void rueckgeldAusgeben(double rueckgabebetrag, byte tickets) {
		byte euro2 = 0, euro1 = 0, cent50 = 0, cent20 = 0, cent10 = 0, cent5 = 0;
		if (rueckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) { // 2 EURO-M�nzen
				//System.out.println("2 EURO");
				euro2++;
				rueckgabebetrag -= 2;
			}
			while (rueckgabebetrag >= 1.0) { // 1 EURO-M�nzen
				//System.out.println("1 EURO");
				euro1++;
				rueckgabebetrag -= 1;
			}
			while (rueckgabebetrag >= 0.5) { // 50 CENT-M�nzen
				//System.out.println("50 CENT");
				cent50++;
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) { // 20 CENT-M�nzen
				//System.out.println("20 CENT");
				cent20++;
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) { // 10 CENT-M�nzen
				//System.out.println("10 CENT");
				cent10++;
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05) { // 5 CENT-M�nzen
				//System.out.println("5 CENT");
				cent5++;
				rueckgabebetrag -= 0.05;
			}
		}
		System.out.println(
				(euro2 >= 1 ? euro2 + "x 2 EURO" : "") + 
				(euro1 >= 1 ? euro1 + "x 1 EURO" : "") + 
				(cent50 >= 1 ? cent50 + "x 50 CENT" : "") + 
				(cent20 >= 1 ? cent20 + "x 20 CENT" : "") + 
				(cent10 >= 1 ? cent10 + "x 10 CENT" : "") + 
				(cent5 >= 1 ? cent5 + "x 5 CENT" : ""));
		
		System.out.println("\nVergessen Sie nicht, " + (tickets > 1 ? "die Fahrscheine" : "den Fahrschein") + "\n"
				+ "vor Fahrtantritt entwerten zu lassen!\n" 
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}
}