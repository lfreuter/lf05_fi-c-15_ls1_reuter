package section1;

public class Konsolenausgabe {

    public static void main(String[] args) {
        /*
         * A1.5
         */

        System.out.println("\n-----------------------\nA1.5.1\n");
        // Aufgabe 1
        System.out.print("Das ist eine \"Aufgabe\".\nDies ist eine Aufgabe.\n");
        // print() ist eine Textausgabe
        // println() ist eine Textausgabe mit einen Zeilenumbruch

        System.out.println("\n-----------------------\nA1.5.2\n");
        // Aufgabe 2
        System.out.printf("%10s%n", "*");
        System.out.printf("%11s%n", "***");
        System.out.printf("%12s%n", "*****");
        System.out.printf("%13s%n", "*******");
        System.out.printf("%14s%n", "*********");
        System.out.printf("%15s%n", "***********");
        System.out.printf("%16s%n", "*************");
        System.out.printf("%11s%n", "***");
        System.out.printf("%11s%n", "***");

        System.out.println("\n-----------------------\nA1.5.3\n");
        // Aufgabe 3
        System.out.printf("%.2f%n", 22.4234234);
        System.out.printf("%.2f%n", 111.2222);
        System.out.printf("%.2f%n", 4.0);
        System.out.printf("%.2f%n", 1000000.551);
        System.out.printf("%.2f%n", 97.34);

        /*
         * A1.6
         */

        System.out.println("\n-----------------------\nA1.6.1\n");
        // Aufgabe 1
        System.out.printf("%4s%n", "**");
        System.out.printf("%s%5s%n", "*", "*");
        System.out.printf("%s%5s%n", "*", "*");
        System.out.printf("%4s%n", "**");

        System.out.println("\n-----------------------\nA1.6.2\n");
        // Aufgabe 2
        System.out.printf("%-5s%s %-19s%s%4s%n", "0!", "=", " ", "=", 1);
        System.out.printf("%-5s%s %-19s%s%4s%n", "1!", "=", "1", "=", 1);
        System.out.printf("%-5s%s %-19s%s%4s%n", "2!", "=", "1 * 2", "=", 2);
        System.out.printf("%-5s%s %-19s%s%4s%n", "3!", "=", "1 * 2 * 3", "=", 6);
        System.out.printf("%-5s%s %-19s%s%4s%n", "4!", "=", "1 * 2 * 3 * 4", "=", 24);
        System.out.printf("%-5s%s %-19s%s%4s%n", "5!", "=", "1 * 2 * 3 * 4 * 5", "=", 120);

        System.out.println("\n-----------------------\nA1.6.3\n");
        // Aufgabe 3
        System.out.printf("%-12s%s%10s%n", "Fahrenheit", "|", "Celcius");
        System.out.printf("%s%n", "-----------------------");
        System.out.printf("%+-12d%s%10.2f%n", -20, "|", -28.8889);
        System.out.printf("%+-12d%s%10.2f%n", -10, "|", -23.3333);
        System.out.printf("%+-12d%s%10.2f%n", 0, "|", -17.7778);
        System.out.printf("%+-12d%s%10.2f%n", 20, "|", -6.6667);
        System.out.printf("%+-12d%s%10.2f%n", 30, "|", -1.1111);
    }
}
