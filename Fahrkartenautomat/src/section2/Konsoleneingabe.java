package section2;

import java.util.Scanner;

public class Konsoleneingabe {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Guten Tag Fremder!");
        System.out.print("Wie lautet denn dein Name? ");

        String name = scanner.nextLine();

        System.out.print("Und wie alt bist du? ");

        int age = scanner.nextInt();

        System.out.print("\nName: " + name + "\n" + "Alter: " + age);
    }

}
