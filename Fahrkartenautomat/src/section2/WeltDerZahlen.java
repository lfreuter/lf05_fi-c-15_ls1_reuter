package section2;

/**
 * Aufgabe: Recherechieren Sie im Internet !
 * 
 * Sie dürfen nicht die Namen der Variablen verändern !!!
 * Vergessen Sie nicht den richtigen Datentyp !!
 *
 * @version 1.0 from 21.08.2019
 * @author Luca-Florian Reuter
 */

public class WeltDerZahlen {

	public static void main(String[] args) {
		/*
		 * *********************************************************
		 * 
		 * Zuerst werden die Variablen mit den Werten festgelegt!
		 * 
		 *********************************************************** 
		 */
		
		// Im Internet gefunden ?
		// Die Anzahl der Planeten in unserem Sonnesystem
		byte anzahlPlaneten = 8;

		// Anzahl der Sterne in unserer Milchstraße
		long anzahlSterne = 40000000000l;

		// Wie viele Einwohner hat Berlin?
		int bewohnerBerlin = 3700000;

		// Wie alt bist du? Wie viele Tage sind das?
		short alterTage = 5908;

		// Wie viel wiegt das schwerste Tier der Welt?
		// Schreiben Sie das Gewicht in Kilogramm auf!
		int gewichtKilogramm = 200000;

		// Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
		int flaecheGroessteLand = 17098242;

		// Wie groß ist das kleinste Land der Erde?
		float flaecheKleinsteLand = 0.44f;

		/*
		 * *********************************************************
		 * 
		 * Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
		 * 
		 *********************************************************** 
		 */

		System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
		System.out.println("Anzahl der Sterne: " + anzahlSterne);
		System.out.println("Einwohner Berlin: " + bewohnerBerlin);
		System.out.println("Alter in Tage: " + alterTage);
		System.out.println("Schwerstes Tier (kg): " + gewichtKilogramm);
		System.out.println("Fläche vom größten Land (km²): " + flaecheGroessteLand);
		System.out.println("Fläche vom kleinsten Land (km²): " + flaecheKleinsteLand);

		System.out.println(" *******  Ende des Programms  ******* ");
	}
}
