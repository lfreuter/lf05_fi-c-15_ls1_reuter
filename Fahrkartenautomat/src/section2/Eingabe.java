package section2;

import java.util.Scanner;

public class Eingabe {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		
		// Die Variable zahl1 speichert die erste Eingabe
		int zahl1= scanner.nextInt(); 
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		
		// Die Variable zahl2 speichert die zweite Eingabe
		int zahl2= scanner.nextInt(); 
		
		// Die Addition der Variablen zahl1 und zahl2 
		// wird der Variable ergebnis zugewiesen.
		
		System.out.print("\n\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + (zahl1 + zahl2));  
		
		System.out.print("\n\nErgebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + (zahl1 - zahl2)); 
		
		System.out.print("\n\nErgebnis der Multiplikation lautet: ");
		System.out.print(zahl1 + " * " + zahl2 + " = " + (zahl1 * zahl2)); 
		
		System.out.print("\n\nErgebnis der Division lautet: ");
		System.out.print(zahl1 + " / " + zahl2 + " = " + (zahl1 / zahl2)); 
		
		scanner.close();
	}
	
}
