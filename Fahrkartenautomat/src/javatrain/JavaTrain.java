package javatrain;

import java.util.Scanner;

public class JavaTrain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int fahrzeit;
		char haltInSpandau;
		char richtungHamburg;
		char haltInStendal = 0;
		char endetIn = 0;
		String endStadt = "Hamburg";

		System.out.print("M�chten Sie in Spandau halten? (j/n): ");
		haltInSpandau = scanner.next().charAt(0);

		System.out.print("M�chten Sie Richtung Hamburg fahren? (j/n): ");
		richtungHamburg = scanner.next().charAt(0);

		if (richtungHamburg == 'n') {
			System.out.print("Wohin m�chten Sie Fahren? (h/b/w): ");
			endetIn = scanner.next().charAt(0);
			
			System.out.print("M�chten Sie in Stendal halten? (j/n): ");
			haltInStendal = scanner.next().charAt(0);
		}
		
		fahrzeit = 8; // Fahrzeit: Berlin Hbf -> Spandau

		if (haltInSpandau == 'j')
			fahrzeit = fahrzeit + 2; // Halt in Spandau

		if (richtungHamburg == 'j')
			fahrzeit += 96;
		else {
			fahrzeit += 34;
			if (haltInStendal == 'j')
				fahrzeit += 16;
			else
				fahrzeit += 6;
			
			if (endetIn == 'h') {
				fahrzeit += 62;
				endStadt = "Hannover";
			} else if (endetIn == 'b') {
				fahrzeit += 50;
				endStadt = "Braunschweig";
			} else if (endetIn == 'w') {
				fahrzeit += 29;
				endStadt = "Wolfsburg";
			}
		}

		System.out.println("Sie erreichen " + endStadt + " nach " + calcTime(fahrzeit));
	}

	private static String calcTime(int time) {
		int hours = time / 60;
		int minutes = time - (hours * 60);
		return (hours >= 1 ? hours + " Stunde(n)" : "") + (minutes >= 1 ? " und " + minutes + " Minute(n)" : "");
	}
}