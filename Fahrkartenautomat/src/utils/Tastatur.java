package utils;
import java.util.Scanner;

public class Tastatur {

	private static Scanner scanner = new Scanner(System.in);

	public static int liesInt() {
		return scanner.nextInt();
	}
	
	public static double liesDouble() {
		return scanner.nextDouble();
	}
	
	public static String liesString() {
		return scanner.next();
	}
	
	public static char liesChar() {
		return scanner.next().charAt(0);
	}
	
	public static byte liesByte() {
		return scanner.nextByte();
	}
	
	public static boolean liesBoolean() {
		return scanner.nextBoolean();
	}
	
}
